#! /bin/env ruby

require 'getoptlong'

gff=nil
features=Array.new
is_length=false
gene_info=Hash.new
genes_included,genes_excluded = []*2
include_list,exclude_list=nil,nil

####################################################
def get_gene_list(gene_file)
  gene_list=Hash.new
  return(gene_list) if gene_file.nil?
  File.open(gene_file,'r').each_line do |line|
    line.chomp!
    gene_list[line]=1
  end
  return gene_list
end

####################################################
opts = GetoptLong.new(
  ['--gff',GetoptLong::REQUIRED_ARGUMENT],
  ['--feature',GetoptLong::REQUIRED_ARGUMENT],
  ['--length',GetoptLong::NO_ARGUMENT],
  ['--include_list',GetoptLong::REQUIRED_ARGUMENT],
  ['--exclude_list',GetoptLong::REQUIRED_ARGUMENT],
)

opts.each do |opt, value|
  case opt
    when '--gff'
      gff=File.expand_path(value)
    when '--feature'
      features.push(value)
    when '--length'
      is_length=true
    when '--include_list'
      include_list=value
    when '--exclude_list'
      exclude_list=value
  end 
end

raise "gff not given" if gff.nil?
raise "feature not given" if features.empty?

####################################################
genes_included=get_gene_list(include_list)
genes_excluded=get_gene_list(exclude_list)

fh = File.open(gff,'r')
while(line=fh.gets) do
  line.chomp!
  next if line =~ /^#/
#NC_007181.1	RefSeq	gene	1294	1629	.	+	.	ID=gene1;Name=Saci_0002;Dbxref=GeneID:3472569;gbkey=Gene;locus_tag=Saci_0002
  genome, feature, start, stop, strand, attributes  = line.split(/\t/).values_at(0,2,3,4,6,8)
  numbered_strand = strand=='+' ? 1 : -1
  if attributes =~ /(?:ID|Parent)=([^; ]+)/ then
    id=$1
    if ! genes_included.empty?
      next if not genes_included.include? id
    end
    if ! genes_excluded.empty?
      next if genes_excluded.include? id
    end

    if features.include? feature then
      gene_info[id]  = gene_info[id].nil? ? Hash.new() : gene_info[id] 
      if gene_info[id]['feature'].nil? then
        gene_info[id]['feature']=Hash.new(0)
      end
      gene_info[id]['feature'][feature]+=1
      gene_info[id]["length"]=0 if gene_info[id]["length"].nil?
      [stop,start].map{|i|i.sub!(/[<>]/,'')}
      gene_info[id]["length"]+=(stop.to_i-start.to_i)+1
    end
  end
end


gene_info.sort_by{|k,v| k}.each do |a|
  id=a[0]
  gene_info[id]['feature'].each_pair do |feature, number|
    print id+"\t"
    print gene_info[id]["length"].to_s+"\t" if is_length
    puts [feature,number].join("\t")
  end
end

