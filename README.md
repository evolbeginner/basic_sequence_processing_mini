# basic_sequence_processing_mini
mini scripts for sequence processing

1. These scripts can be used to convert sequence format, delete sequences based on certain criteria, rename sequences, etc.

2. Please type python/perl/ruby ScriptName -h or read the codes directly to see the usage of these scripts. 

3. If you have any question/suggestion, please write e-mails to sishuowang@hotmail.ca. Your help is highly appreciated!
