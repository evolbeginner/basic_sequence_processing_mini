#! /bin/env ruby

# to create randomized seqs
# Last updated: 2014-08-19
# Sishuo Wang (sishuowang@hotmail.ca) from The University of British Columbia

######################################################
require 'bio'
require 'getoptlong'

######################################################
def generate_randomized_seq(input, pasted_seqs, is_rename=false, additional_seq=nil)
  input=input;
  pasted_seqs=pasted_seqs;
  randomized_seqs=Hash.new
  if input then
    fh = Bio::FlatFile.open(input)
    fh.each_entry do |f|
      $counter+=1
      seq_obj=Bio::Sequence.auto(f.seq)
      seq_obj=Bio::Sequence.auto(f.seq)
      if is_rename then
        randomized_seqs[$counter]=seq_obj.randomize
      else
        randomized_seqs[f.definition]=seq_obj.randomize
      end
    end
  elsif pasted_seqs
    pasted_seqs.each do |seq|
      seq_obj=Bio::Sequence.auto(seq)
      $counter+=1
      randomized_seqs[$counter]=seq_obj.randomize
    end
  else
    raise "no input or pasted given!"
  end

  if additional_seq
    #randomized_seqs.each_key {|seq_header| randomized_seqs[seq_header] = additional_seq+randomized_seqs[seq_header]}
    randomized_seqs.each_key {|seq_header| randomized_seqs[seq_header] += additional_seq }
  end

  return(randomized_seqs)
end


def usage
  puts "USAGE:"
  puts "ruby #{$0} Options"
  print <<EOF
Mandantary options:
  one of the following two
    -i|--input <input>
    -pasted_seq
Optional options:
  --max_length: default the length of each sequence
  --times:  default 1
  --additional_seq: additional seq that will be added to each randomized seq
                    default: off
  -h|--help
EOF
  puts
  puts "Please do not hesitate to write e-mail to sishuowang@hotmail.ca if you have any questions or suggestions. Thanks!"
end

######################################################
input = nil
max_length = nil
times=1
randomized_seqs=Hash.new
pasted_seqs=Array.new
is_rename=false
$counter=0
additional_seq=nil

usage if ARGV.empty?

opts=GetoptLong.new(
  ['-i', '--input', GetoptLong::REQUIRED_ARGUMENT],
  ['--pasted_seq',GetoptLong::REQUIRED_ARGUMENT],
  ['--max_length', GetoptLong::REQUIRED_ARGUMENT],
  ['--times', GetoptLong::REQUIRED_ARGUMENT],
  ['--rename', GetoptLong::NO_ARGUMENT],
  ['--additional_seq', GetoptLong::REQUIRED_ARGUMENT],
  ['-h','--help', GetoptLong::NO_ARGUMENT],
)

opts.each do |opt,value|
  case opt
    when '-i', '--input'
      input=value
    when '--max_length'
      max_length=value.to_i
    when '--times'
      times=value.to_i
    when '--pasted_seq'
      pasted_seqs.push value
    when '--rename'
      is_rename=true
    when '--additional_seq'
      additional_seq = value
    when '-h', '--help'
      usage
  end
end

######################################################
times.times do 
  randomized_seqs=generate_randomized_seq(input,pasted_seqs,is_rename,additional_seq)
  randomized_seqs.each_pair do |title, seq|
    length = max_length.nil? ? (seq.length) : (max_length)
    puts ">#{title}", seq[0,length]
  end
end


