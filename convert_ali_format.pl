#! /usr/bin/perl

use Bio::AlignIO;
use strict;

my ($inputfilename, $informat, $outformat) = @ARGV;
die "must provide input file as 1st parameter...\n" unless $inputfilename;
my $in  = Bio::AlignIO->new(-file   => $inputfilename ,
                         -format => $informat,
			 -idlength => 100,
                         -interleaved => 1);
my $out = Bio::AlignIO->new(-fh   => \*STDOUT ,
                         -format => $outformat);

while ( my $aln = $in->next_aln() ) {
    $out->write_aln($aln);
}

