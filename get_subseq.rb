#! /bin/env ruby

# to get sub-sequences, select sequences based on sequence names and obtain regions of sequences
# written by Sishuo Wang from the department of Botany, the University of British Columbia
# Please write e-mails to sishuowang@hotmail.ca if you have any question and/or suggestion.

#############################################################################
require 'bio';
require 'getoptlong'

starting=nil
ending=nil
region=nil
infile=nil
seq_titles_included=[]
seq_title_patterns_included=[]
is_real_posi=false
alignment_obj=nil
output_format=nil
is_rev=false
is_nogap=false
is_single_line=false


#############################################################################
class Bio::Sequence::NA
  def strict_complement
    seq.tr('ATGCUatgcu','TACGAtacga')
  end
end

#############################################################################
def read_seq_name(seq_title_patterns_included, seq_titles_included, value)
  if value =~ /^ \/ (.+) \/ $/x then
    regexp = Regexp.new($1)
    seq_title_patterns_included.push(regexp)
  else
    seq_titles_included.push(value)
  end
end


def process_seq(title, seq, alignment_obj, seq_title_patterns_included, seq_titles_included, starting, ending, is_real_posi, is_rev=false, is_nogap=false)
  exister=0
  if ! (seq_titles_included.empty? and seq_title_patterns_included.empty?) then
    if ! seq_titles_included.empty? then 
      if seq_titles_included.include?(title) then exister+=1 end
    end
    if ! seq_title_patterns_included.empty? then
      seq_title_patterns_included.each do |pattern|
        if title =~ pattern then exister+=1; end
      end
    end
    exister == 0 ? (return) : (;)
  end

  seq_obj = Bio::Sequence.auto(seq)
  if is_real_posi then seq_obj.seq.gsub!(/[-~]/,'') end
  if is_rev
      begin
        seq = seq_obj.strict_complement
      rescue
        raise "seq #{title} is not NA sequence"
      end
  end

  starting=1 if not starting;
  ending=seq_obj.seq.length if not ending
  final_seq = is_rev ? seq[starting-1,ending-starting+1].reverse : seq[starting-1,ending-starting+1]
  final_seq.gsub!('-','') if is_nogap
  alignment_obj.add_seq(final_seq, title)
end


def usage
  script_basename=File.basename $0
  puts "The usage of #{script_basename}: [Options]"
  print <<EOF
ruby get_subseq.rb
Mandatory arguments
  <-i|--in|--infile>                infile_name
Optional arguments:
  [--sub|--region|--subseq]         should be in the format of start-end, e.g. 10-100
  [--seq_name|--seq|--seq_included] names of sequences
  [--seq_name_from_file|
   --seq_from_file|
   --seq_included_from_file]        the list of names of sequences from a file
  [--real|--real_posi]              extract sequences based on real positions (without counting gaps) of sequences
  [--output_format]                 format of output
  [--rev]                           reverse complement?
                                    default: off
  [--nogap|--no_gap]                remove gap
                                    default: off
  [--single_line]                   output in single line for each sequence
                                    default: off
  [-h|--help]                       usage
EOF
  exit
end


#############################################################################
usage if ARGV.empty?

opts=GetoptLong.new(
  ['-i', '--in', '--infile', GetoptLong::REQUIRED_ARGUMENT],
  ['--sub', '--region', '--subseq', GetoptLong::REQUIRED_ARGUMENT],
  ['--seq_name', '--seq', '--seq_included', GetoptLong::REQUIRED_ARGUMENT],
  ['--seq_name_from_file', '--seq_from_file', "--seq_included_from_file", GetoptLong::REQUIRED_ARGUMENT],
  ['--real', '--real_posi', GetoptLong::NO_ARGUMENT],
  ['--output_format', GetoptLong::REQUIRED_ARGUMENT],
  ['--rev', GetoptLong::NO_ARGUMENT],
  ['--nogap', '--no_gap', GetoptLong::NO_ARGUMENT],
  ['--single_line', GetoptLong::NO_ARGUMENT],
  ['-h','--help', GetoptLong::NO_ARGUMENT],
)

opts.each do |opt, value|
  case opt
    when '-i', '--in', '--infile'
      infile = value =~ /^-$/ ? (ARGF) : (value)
    when '--sub', '--region', '--subseq'
      region=value
      if region !~ /^ \s*(\d+)\s* [-] \s*(\d+)\s* $/x then
        raise "region format error";
      else
        starting=$1.to_i; ending=$2.to_i
      end
    when '--seq_name', '--seq', '--seq_included'
        read_seq_name(seq_title_patterns_included, seq_titles_included, value)
    when '--seq_name_from_file', '--seq_from_file', '--seq_included_from_file'
        File.open(value,'r') do |file|
          while line=file.gets do
            line.chomp!
            read_seq_name(seq_title_patterns_included, seq_titles_included, line)
          end
        end
    when '--real', '--real_posi'
      is_real_posi=TRUE
    when '--output_format'
      output_format=value
    when '--rev'
      is_rev=true
    when '--nogap', '--no_gap'
      is_nogap = true
    when '--single_line'
      is_single_line = true
    when '-h', '--help'
      usage()
  end
end

output_format ||= "fasta"
alignment_obj = Bio::Alignment::OriginalAlignment.new() 


#############################################################################
a=Bio::Alignment::OriginalAlignment.readfiles(infile)
a.each_pair do |title, seq|
  process_seq(title, seq, alignment_obj, seq_title_patterns_included, seq_titles_included, starting, ending, is_real_posi, is_rev, is_nogap)
end

if is_single_line
  alignment_obj.each_pair do |seq_title, seq|
    puts ">"+seq_title+"\n"
    puts seq
  end
else
  puts alignment_obj.output(:"#{output_format}", {})
end


