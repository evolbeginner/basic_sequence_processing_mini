#! /bin/env ruby

require 'bio'
require 'getoptlong'

######################################################
input=nil
new_features=Array.new
separator="|"

opts=GetoptLong.new(
  ['-i','--input',GetoptLong::REQUIRED_ARGUMENT],
  ['--new_feature','--new_features',GetoptLong::REQUIRED_ARGUMENT],
  ['--separator',GetoptLong::REQUIRED_ARGUMENT],
)

opts.each do |opt,value|
  case opt
    when '-i', '--input'
      input=value
    when '--new_feature', '--new_features'
      new_features.push value.split(',')
    when '--separator'
      separator=value
  end
end

raise "new_features have to be given" if new_features.empty?
new_features.flatten!

######################################################
fh=Bio::FlatFile.open(input)
fh.each_entry do |f1|
  new_name_array=Array.new
  fasta_string=f1.definition+"\n"+f1.seq
  f = Bio::FastaFormat.new(fasta_string)
  new_name = new_features.map{|i| new_name_array.push f.send(i.to_sym)}.join(separator)
  puts ">"+new_name
  puts f.seq
end


