#! /bin/env ruby

require 'bio'

seq_titles = Hash.new

Bio::FlatFile.open(ARGV[0]).each_entry do |f|
  if seq_titles.include? f.definition
    puts f.definition
  end
  seq_titles[f.definition] = ''
end


