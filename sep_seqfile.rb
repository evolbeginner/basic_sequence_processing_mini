#! /bin/env ruby

# separate a fasta file according to the number of seqs

require 'getoptlong'
require 'bio'
require 'SSW_bio'
require 'Dir'

####################################################
input = nil
outdir = nil
num = nil
force = false
is_on_average = true

outputs = Array.new
seq_objs = Hash.new
seq_names = Array.new
seq_names_hash = Hash.new{|h,k|h[k]=Array.new}
num_of_total_seqs = nil

opts = GetoptLong.new(
  ['-i', '--in', GetoptLong::REQUIRED_ARGUMENT],
  ['-o', '--out', '--outdir', GetoptLong::REQUIRED_ARGUMENT],
  ['--num', GetoptLong::REQUIRED_ARGUMENT],
  ['--force', GetoptLong::NO_ARGUMENT],
)

opts.each do |opt,value|
  case opt
    when '-i', '--in'
      input = value
    when '-o', '--out', '--outdir'
      outdir = value
    when '--num'
      num = value.to_i
    when '--force'
      force = true
  end
end

raise "input not given! Exiting ......" if not input
raise "output/outdir not given! Exiting ......" if not outdir
raise "num not given! Exiting ......" if not num

mkdir_with_force(outdir, force)

1.upto(num){|i|
  basename = File.basename(input)
  outputs << File.join(outdir, basename + "_" + i.to_s)
}

##############################################################
seq_objs = read_seq_file(input)

seq_names = seq_objs.keys
num_of_total_seqs = seq_names.size

num_per_output = (num_of_total_seqs.to_f/num).ceil
1.upto(num) do |i|
  break if seq_names.empty?
  1.upto(num_per_output) do
    break if seq_names.empty?
    seq_names_hash[i] << seq_names.shift
  end
end

# generate output sequences
seq_names_hash.each_key do |index|
  output = outputs.shift
  out_fh = File.open(output, 'w')
  value = seq_names_hash[index]
  value.each do |i|
    out_fh.puts ">"+i
    out_fh.puts seq_objs[i].seq
  end
  out_fh.close
end


