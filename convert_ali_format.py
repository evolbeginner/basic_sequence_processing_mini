#! /usr/local/bin/python

# for file format converting
# For more information about the format see http://biopython.org/wiki/AlignIO#File_Formats
# 'phylip-relaxed' is an option instead of 'phylip'

from Bio import AlignIO
import getopt
import sys
import re

#############################################################
def show_help():
    print ''' python2.7 convert_ali_format.py <--in=infile> <--out=outfile> <--in_fmt=infile_format> <--out_fmt=outfile_format> [--h|--help]
    '''
    sys.exit(-1)

try:
    opts, args = getopt.getopt(sys.argv[1:], "h", ["help","in=","in_fmt=","out=","out_fmt="])
except getopt.GetoptError:
    print "Illegal params!"
    print
    show_help()
for op, value in opts:
    if op == "--in":
        infile = value
    elif op == "--out":
        outfile = value
    elif op == "--in_fmt":
        in_fmt = value
    elif op == "--out_fmt":
        out_fmt = value
    elif re.search('^--?h(elp)?$',op):
        show_help()
    
#############################################################
input_handle = open(infile, "rU")
output_handle = open(outfile, "w")
 
alignments = AlignIO.parse(input_handle, in_fmt)
AlignIO.write(alignments, output_handle, out_fmt)
 
output_handle.close()
input_handle.close()

